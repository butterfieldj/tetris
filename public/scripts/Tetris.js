// Found Tetris game design and organization at link, used it for guidance
// https://github.com/maxwihlborg/youtube-tutorials/tree/master/tetris/js
MYGAME.tetris = (function () {
    'use strict';

    var gameboard,
        stats,
        currentPiece,
        nextPiece = {},
        factory,
        elapsedTime = 0,
        done = false,
        floodArr = [];

    function init() {
        gameboard = MYGAME.gameboard();
        gameboard.init();

        stats = MYGAME.stats();
        stats.init();

        factory = MYGAME.pieceFactory();
        currentPiece = factory.createPiece();
        nextPiece = factory.createPiece();
    }

    function getNewPiece() {
        if (!done && nextPiece.check(gameboard.contents(), 0, 0)) {
            currentPiece = nextPiece;
            nextPiece = factory.createPiece();
        } else {
            done = true;
        }
    }

    function alreadyFilled(x, y) {
        // this functions checks to see if our square has been filled already
        if (x >= gameboard.rows || x < 0) {
            return true;
        } else if (y >= gameboard.cols || y < 0) {
            return true;
        } else if (gameboard.contents()[x][y].id === currentPiece.getInfo().id) {
            return true;
        } else if (gameboard.contents()[x][y].color === 'white') {
            return true;
        } else if (floodArr[x][y] !== 0) {
            return true;
        }

        return false;
    }

    function floodFill(x, y, n) {
        if (alreadyFilled(x, y)) {
            return false;
        }

        floodArr[x][y] = n;

        floodFill(x, y - 1, n);
        floodFill(x + 1, y, n);
        floodFill(x, y + 1, n);
        floodFill(x - 1, y, n);

        return true;
    }

    function chainReaction() {
        var i,
            j,
            k,
            r,
            c,
            n = 1,
            tempArr,
            regionShape,
            loc = {x: 0, y: 0},
            emptyRow,
            locSet,
            regionPiece,
            spaces,
            contents,
            regionTop,
            regionDropped;

        floodArr = [];

        // Create Empty array of zeros
        for (i = 0; i < gameboard.rows; i++) {
            tempArr = [];
            for (j = 0; j < gameboard.cols; j++) {
                tempArr.push(0);
            }
            floodArr.push(tempArr);
        }

        // Iterate through whole gameboard and flood fill it
        for (i = gameboard.rows - 1; i >= 0; i--) {
            for (j = 0; j < gameboard.cols; j++) {
                if (floodFill(i, j, n)) {
                    n++;
                }
            }
        }

        n = n - 1;
        regionDropped = false;

        for (k = 1; k <= n; k++) {
            regionShape = [];
            locSet = false;
            for (i = 0; i < gameboard.rows; i++) {
                tempArr = [];
                emptyRow = true;
                for (j = 0; j < gameboard.cols; j++) {
                    if (floodArr[i][j] !== k) {
                        tempArr.push(0);
                    } else {
                        emptyRow = false;
                        tempArr.push(1);
                        if (!locSet) {
                            loc.x = i;
                            locSet = true;
                        }
                    }
                }
                if (!emptyRow) {
                    regionShape.push(tempArr);
                }
            }
            regionPiece = MYGAME.piece({color: 'other', id: k, shape: regionShape});
            regionPiece.setLoc(loc.x, loc.y);

            if (regionPiece.chainCheck(floodArr, 1, 0)) {
                spaces = 0;
                regionDropped = true;
                while (regionPiece.chainCheck(floodArr, 1, 0)) {
                    regionPiece.drop();
                    spaces++;
                }

                // Go through gameboard and drop region n spaces
                contents = gameboard.contents();
                regionTop = regionPiece.getInfo().loc.x - spaces;
                for (r = gameboard.rows - 1; r >= regionTop; r--) {
                    for (c = 0; c < gameboard.cols; c++) {
                        if (floodArr[r - spaces][c] === k) {
                            contents[r][c].color = contents[r - spaces][c].color;
                            contents[r][c].id = contents[r - spaces][c].id;
                            contents[r - spaces][c].color = 'white';
                            contents[r - spaces][c].id = 0;
                        }
                    }
                }
                gameboard.setContents(contents);
            }
        }
        return regionDropped;
    }

    function removeRow(row) {
        var i, j, contents = gameboard.contents();
        for (i = row; i >= 0; i--) {
            for (j = 0; j < gameboard.cols; j++) {
                if (i === 0) {
                    contents[i][j].color = 'white';
                } else {
                    if (contents[i - 1][j].id !== currentPiece.getInfo().id) {
                        contents[i][j].color = contents[i - 1][j].color;
                    }
                }
            }
        }

        gameboard.setContents(contents);
    }

    function removeRows() {
        var i,
            j,
            contents = gameboard.contents(),
            full;

        for (i = gameboard.rows - 1; i >= 0; i--) {
            full = true;
            for (j = 0; j < gameboard.cols; j++) {
                if (contents[i][j].color === 'white') {
                    full = false;
                    break;
                }
            }

            if (full) {
                console.log('Row ' + i + ' Full');
                removeRow(i);
                chainReaction();
                i++;
            }
        }
    }

    function checkRows() {
        var full = false,
            removed = 0,
            i,
            j,
            contents = gameboard.contents(),
            removedArray = [];

        for (i = gameboard.rows - 1; i >= 0; i--) {
            full = true;
            for (j = 0; j < gameboard.cols; j++) {
                if (contents[i][j].color === 'white') {
                    full = false;
                    break;
                }
            }

            if (full) {
                console.log('Row ' + i + ' Full');
                removedArray.push(i);
                removed++;
                stats.addLine();
            }
        }

        if (removedArray.length > 0) {
            stats.addScore(removed);
            stats.checkLevelUp();
        }

        return removedArray;
    }

    function setPiece() {
        var removedArray = [];
        gameboard.setContents(currentPiece.addToGrid(gameboard.contents()));
        removedArray = checkRows();
        getNewPiece();

        return removedArray;
    }

    function moveLeft() {
        if (currentPiece.check(gameboard.contents(), 0, -1)) {
            currentPiece.clearPreviousPosition(gameboard.contents());
            currentPiece.moveLeft();
        }
    }

    function moveRight() {
        if (currentPiece.check(gameboard.contents(), 0, 1)) {
            currentPiece.clearPreviousPosition(gameboard.contents());
            currentPiece.moveRight();
        }
    }

    function rotateClockwise() {
        if (currentPiece.checkRotate(gameboard, 'R')) {
            currentPiece.clearPreviousPosition(gameboard.contents());
            currentPiece.rotateClock();
        }
    }

    function rotateCounterClockwise() {
        if (currentPiece.checkRotate(gameboard, 'L')) {
            currentPiece.clearPreviousPosition(gameboard.contents());
            currentPiece.rotateCounter();
        }
    }

    function softDrop() {
        var removedArray = [];
        if (currentPiece.check(gameboard.contents(), 1, 0)) {
            currentPiece.clearPreviousPosition(gameboard.contents());
            currentPiece.drop();
        } else {
            removedArray = setPiece();
        }

        return removedArray;
    }

    function hardDrop() {
        var rows = 0, removedArray = [];
        while (currentPiece.check(gameboard.contents(), 1, 0)) {
            currentPiece.clearPreviousPosition(gameboard.contents());
            currentPiece.drop();
            rows++;
        }
        stats.addRowsDropped(rows);
        removedArray = setPiece();

        return removedArray;
    }

    function getCurrentView() {
        var c = gameboard.contents().slice();
        return currentPiece.addToGrid(c);
    }

    function getNextPiece() {
        return nextPiece.getInfo();
    }

    function getCurrentPiece() {
        return currentPiece;
    }

    function getStatus() {
        return done;
    }

    function update(time) {
        var removedArray = [];
        elapsedTime += time;
        if (elapsedTime > stats.getDropTime()) { // Decreases at a rate of level * 0.075 until level 10 is reached
            elapsedTime = 0;
            removedArray = softDrop();
        }
        return removedArray;
    }

    function getStats() {
        return stats.getStats();
    }

    return {
        init: init,
        moveLeft: moveLeft,
        moveRight: moveRight,
        rotateClockwise: rotateClockwise,
        rotateCounterClockwise: rotateCounterClockwise,
        softDrop: softDrop,
        hardDrop: hardDrop,
        getCurrentView: getCurrentView,
        getNextPiece: getNextPiece,
        getCurrentPiece: getCurrentPiece,
        update: update,
        getStatus: getStatus,
        getStats: getStats,
        removeRows: removeRows,
        checkRows: checkRows
    };
});
