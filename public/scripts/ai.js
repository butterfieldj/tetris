// AI methodology adapted from https://codemyroad.wordpress.com/2013/04/14/tetris-ai-the-near-perfect-player/
MYGAME.ai = (function () {
    'use strict';

    var virtualContents, // Holds the virtual gameboard, used for finding score of placement
        actualContents, // Actual state of the board
        currentPiece;

    function moveLeft() {
        if (currentPiece.check(virtualContents, 0, -1)) {
            currentPiece.clearPreviousPosition(virtualContents);
            currentPiece.moveLeft();
        }
    }

    function moveRight() {
        if (currentPiece.check(virtualContents, 0, 1)) {
            currentPiece.clearPreviousPosition(virtualContents);
            currentPiece.moveRight();
        }
    }

    function rotateClockwise() {
        if (currentPiece.checkVirtualRotate(virtualContents, 'R')) {
            currentPiece.clearPreviousPosition(virtualContents);
            currentPiece.rotateClock();
        }
    }

    function hardDrop() {
        while (currentPiece.check(virtualContents, 1, 0)) {
            currentPiece.clearPreviousPosition(virtualContents);
            currentPiece.drop();
        }

        setPiece();
    }

    // Adds the piece to the grid
    function setPiece() {
        currentPiece.addToGrid(virtualContents);
    }

    // Copy function, resets the board to it original state, before the piece was dropped
    function resetVirtualBoard() {
        var arr = [], tempA, row, col;

        for (row = 0; row < actualContents.length; row++) {
            tempA = [];
            for (col = 0; col < actualContents[row].length; col++) {
                tempA.push({
                    color: actualContents[row][col].color,
                    id: actualContents[row][col].id
                });
            }
            arr.push(tempA);
        }

        virtualContents = arr;
    }

    function rotatePiece(rotations) {
        var i;
        for (i = 0; i < rotations; i++) {
            rotateClockwise();
        }
    }

    // Restarts the piece where it was initially, and resets its shape (no rotation)
    function resetCurrentPiece(shape, initialX, initialY) {
        currentPiece.setShape(shape);
        currentPiece.setLoc(initialX, initialY);
    }

    // Calculates the height of a desired column
    function getColumnHeight(col) {
        var row;
        for (row = 0; row < virtualContents.length; row++) {
            // Find which row the highest block is at
            if (virtualContents[row][col].color !== 'white') {
                return virtualContents.length - row;
            }
        }

        return 0;
    }

    // Calculates height of all of the columns
    function getAggregateHeight() {
        var col, aggregateHeight = 0;

        for (col = 0; col < virtualContents[0].length; col++) {
            aggregateHeight += getColumnHeight(col);
        }

        return aggregateHeight;
    }

    // Calculates the "bumpiness" of the board
    function getBumpiness() {
        var col, bumpiness = 0;
        for (col = 0; col < virtualContents[0].length - 1; col++) {
            bumpiness += Math.abs(getColumnHeight(col) - getColumnHeight(col + 1));
        }

        return bumpiness;
    }

    // Determines if a row is complete
    function isCompleteRow(row) {
        var col;
        for (col = 0; col < virtualContents[row].length; col++) {
            if (virtualContents[row][col].color === 'white') {
                return false;
            }
        }

        return true;
    }

    // Calculates the total number of complete lines
    function getCompleteLines() {
        var row, completeRows = 0;
        for (row = 0; row < virtualContents.length; row++) {
            if (isCompleteRow(row) === true) {
                completeRows++;
            }
        }

        return completeRows;
    }

    // Finds the number of holes in the grid, algorithm is from the AI tutorial
    function getHoles() {
        var row, col, holes = 0, block;

        for (col = 0; col < virtualContents[0].length; col++) {
            block = false;
            for (row = 0; row < virtualContents.length; row++) {
                if (virtualContents[row][col].color !== 'white') {
                    block = true;
                } else if (block === true) {
                    holes++;
                }
            }
        }

        return holes;
    }

    // These special constants were given in the tutorial
    function getMoveScore() {
        return -0.66569 * getAggregateHeight() +
            0.99275 * getCompleteLines() +
            -0.46544 * getHoles() +
            -0.24077 * getBumpiness();
    }

    // Finds the best move
    function getBestMove(board, current) {
        var initialX = current.getInfo().loc.x,
            initialY = current.getInfo().loc.y,
            shape = current.getInfo().shape,
            right,
            i,
            j,
            rotations,
            tempScore,
            best = {
                score: -10000000,
                right: 0,
                rotations: 0
            };

        actualContents = board;
        currentPiece = current;

        // Check at each rotation
        for (rotations = 0; rotations < 4; rotations++) {
            // Check at each position
            for (right = 0; right <= 10; right++) {

                // Reset position
                resetVirtualBoard();
                resetCurrentPiece(shape, initialX, initialY);

                // Rotate piece
                rotatePiece(rotations);

                // Start at the left
                for (j = 0; j < 6; j++) {
                    moveLeft();
                }

                // Move right
                for (i = 0; i < right; i++) {
                    moveRight();
                }

                // Drop the piece into position
                hardDrop();

                // Get scores
                tempScore = getMoveScore();

                // Keep details of the best score
                if (tempScore > best.score) {
                    best.score = tempScore;
                    best.right = right;
                    best.rotations = rotations;
                }
            }
        }

	// Reset the current piece to its initial position
        resetCurrentPiece(shape, initialX, initialY);

        return best;
    }

    return {
        getBestMove: getBestMove
    };
}());
