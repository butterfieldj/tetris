MYGAME.screens['attract-mode'] = (function () {
    'use strict';

    var logoImage = {
            x: 0,
            y: 0,
            width: 789,
            height: 55
        },
        backgroundImage = {
            x: 0,
            y: 0,
            width: 0,
            height: 0
        },
        gameOverImage = {
            x: 0,
            y: 0,
            width: 200,
            height: 100
        },
        game,
        cancelled,
        id,
        lastTime,
        blockMovingThreshold,
        currentPieceId,
        boardContainer = {
            x: 250,
            y: 280,
            width: 220,
            height: 420,
            fillStyle: "#4A4A4A"
        },
        scoreImage = {
            x: boardContainer.x + boardContainer.width,
            y: 0,
            width: 150,
            height: 35
        },
        scoreContainer = {
            x: 650,
            y: 0,
            width: 150,
            height: 50,
            fillStyle: "#4A4A4A",
            font: '30px Georgia',
            altFill: "#FFCC00",
            text: ""
        },
        levelImage = {
            x: scoreImage.x,
            y: 0,
            width: 140,
            height: 35
        },
        levelContainer = {
            x: 650,
            y: 0,
            width: 150,
            height: 50,
            fillStyle: "#4A4A4A",
            font: '30px Georgia',
            altFill: "#FFCC00",
            text: ""
        },
        rowsClearedImage = {
            x: scoreImage.x,
            y: 0,
            width: 185,
            height: 53
        },
        rowsClearedContainer = {
            x: 650,
            y: 0,
            width: 150,
            height: 50,
            fillStyle: "#4A4A4A",
            font: '30px Georgia',
            altFill: "#FFCC00",
            text: ""
        },
        nextPieceImage = {
            x: boardContainer.x + boardContainer.width + 100,
            y: 0,
            width: 300,
            height: 30
        },
        nextPieceContainer = {
            x: nextPieceImage.x,
            y: 0,
            width: 250,
            height: 100,
            fillStyle: "#4A4A4A"
        },
        demoModeGraphic = {
            x: 0,
            y: 0,
            width: 100,
            height: boardContainer.height,
            fillStyle: "#4A4A4A",
            font: 'bold 30px Arial',
            altFill: "#FFCC00",
            text: "AI DEMO MODE"
        },
        demoModeShow,
        demoModeTime,
        size = 20,
        gameOver,
        rotations,
        leftMoves,
        rightMoves,
        animateParticles,
        removedArray,
        colors = [],
        goBack = function () {
	    MYGAME.sounds['audio/battleOfHeroes.wav'].pause();
       	    MYGAME.sounds['audio/battleOfHeroes.wav'].currentTime = 0;

            document.removeEventListener('mousemove', goBack, false);
            document.removeEventListener('keydown', goBack, false);
            cancelled = true;
            if (id) {
                window.cancelAnimationFrame(id);
            }
            MYGAME.game.showScreen('main-menu');
        };

    var moveRight = function (elapsedTime) {
        game.moveRight();
    };

    var moveLeft = function (elapsedTime) {
        game.moveLeft();
    };

    var rotateRight = function (elapsedTime, reset) {
        game.rotateClockwise();
    };

    var hardDrop = function (elapsedTime, reset) {
        removedArray = game.hardDrop();

        if (removedArray.length > 0) {
            cancelled = true;
            animateParticles = true;
        }
    };

    var softDrop = function (elapsedTime) {
        removedArray = game.softDrop();
        if (removedArray.length > 0) {
            cancelled = true;
            animateParticles = true;
        }
    };

    // When the game loads
    function initialize() {
        var canvasWidth = MYGAME.graphics.canvas.width,
            canvasHeight = MYGAME.graphics.canvas.height,
            yOffset = canvasHeight / 6;

        backgroundImage.image = MYGAME.images['graphics/vader_clone_background.jpg'];
        backgroundImage.x = canvasWidth / 2;
        backgroundImage.y = canvasHeight / 2;
        backgroundImage.width = canvasWidth;
        backgroundImage.height = canvasHeight;

        logoImage.image = MYGAME.images['graphics/logo.png'];
        logoImage.y = yOffset - logoImage.height;
        logoImage.x = canvasWidth / 2;

        scoreImage.image = MYGAME.images['graphics/score.png'];
        scoreImage.y = yOffset + 10;

        scoreContainer.y = scoreImage.y;

        levelImage.image = MYGAME.images['graphics/level.png'];
        levelImage.y = yOffset * 2 + 10;

        levelContainer.y = levelImage.y;

        rowsClearedImage.image = MYGAME.images['graphics/rowsCleared.png'];
        rowsClearedImage.y = yOffset * 3 + 10;

        rowsClearedContainer.y = rowsClearedImage.y;

        nextPieceImage.image = MYGAME.images['graphics/nextPiece.png'];
        nextPieceImage.y = yOffset * 4 + 10;

        nextPieceContainer.y = nextPieceImage.y + nextPieceContainer.height - 10;

        gameOverImage.image = MYGAME.images['graphics/gameOver.png'];
        gameOverImage.x = boardContainer.x;
        gameOverImage.y = boardContainer.y;

        demoModeGraphic.x = canvasWidth / 2;
        demoModeGraphic.y = canvasHeight / 2;

        colors.white = MYGAME.images['graphics/backgroundSquare.png'];
        colors.purple = MYGAME.images['graphics/purpleSquare.png'];
        colors.blue = MYGAME.images['graphics/blueSquare.png'];
        colors.green = MYGAME.images['graphics/greenSquare.png'];
        colors.yellow = MYGAME.images['graphics/yellowSquare.png'];
        colors.cyan = MYGAME.images['graphics/cyanSquare.png'];
        colors.red = MYGAME.images['graphics/redSquare.png'];
        colors.orange = MYGAME.images['graphics/orangeSquare.png'];
    }

    // When the AI starts
    function run() {
        var best;

        // Pattern for general event listeners found at
        // http://stackoverflow.com/questions/13206042/detecting-mouse-movement-without-jquery/13206146#13206146
        document.addEventListener('mousemove', goBack, false);
        document.addEventListener('keydown', goBack, false);

	MYGAME.sounds['audio/battleOfHeroes.wav'].currentTime = 0;
	MYGAME.sounds['audio/battleOfHeroes.wav'].play();

        removedArray = [];

        gameOver = false;
        animateParticles = false;

        game = MYGAME.tetris();
        game.init();
        currentPieceId = game.getCurrentPiece().getInfo().id;
        blockMovingThreshold = 0;

        demoModeTime = 0;
        demoModeShow = false;

        best = MYGAME.ai.getBestMove(game.getCurrentView(), game.getCurrentPiece()); // Find the best position for the first piece

	// Set the moves for the first piece
        rotations = best.rotations;
        leftMoves = 6;
        rightMoves = best.right;

        cancelled = false;
        lastTime = performance.now();
        id = window.requestAnimationFrame(gameLoop);
    }

    // Initialize particles
    function startParticles() {
        var board = game.getCurrentView(),
            i, j, p, startX = boardContainer.x - boardContainer.width / 2.1 + 15,
            startY = boardContainer.y - boardContainer.height / 2.1 + 10,
            initialX = startX;

        MYGAME.particles.reset();
        for (i = 0; i < board.length; i++) {
            startX = initialX;

            for (j = 0; j < board[i].length; j++) {
                if (removedArray.indexOf(i) !== -1) {
                    MYGAME.graphics.drawImage({
                        image: colors.white,
                        x: startX,
                        y: startY,
                        width: size,
                        height: size
                    });

                    for (p = 0; p <= 10; p++) {
                        MYGAME.particles.createParticle({
                            image: MYGAME.images['graphics/smoke.png'],
                            size: 20,
                            x: startX,
                            y: startY,
                            duration: 0.25,
                            deviation: 0.1,
                            speed: 1,
                            speedDev: 0.5
                        });
                    }

                    for (p = 0; p <= 8; p++) {
                        MYGAME.particles.createParticle({
                            image: colors[board[i][j].color],
                            size: 5,
                            x: startX,
                            y: startY,
                            duration: 0.2,
                            deviation: 0.1,
                            speed: 80,
                            speedDev: 1
                        });
                    }

                    for (p = 0; p <= 8; p++) {
                        MYGAME.particles.createParticle({
                            image: MYGAME.images['graphics/fire.png'],
                            size: 15,
                            x: startX,
                            y: startY,
                            duration: 0.2,
                            deviation: 0.1,
                            speed: 2,
                            speedDev: 0.5
                        });
                    }

                    startX += size;
                }
            }

            startY += size;
        }

        lastTime = performance.now();
        id = window.requestAnimationFrame(particleLoop);
        MYGAME.sounds['audio/Laser_Blaster.wav'].play();
    }

    // Update the positions of the particles, check if they have expired
    function updateParticles(elapsedTime) {
        animateParticles = MYGAME.particles.update(elapsedTime);
    }

    // Draw the particles
    function renderParticles(elapsedTime) {
        var board = game.getCurrentView(),
            i, j, startX = boardContainer.x - boardContainer.width / 2.1 + 15,
            startY = boardContainer.y - boardContainer.height / 2.1 + 10,
            initialX = startX;

        render(elapsedTime);

        for (i = 0; i < board.length; i++) {
            startX = initialX;

            for (j = 0; j < board[i].length; j++) {
                if (removedArray.indexOf(i) !== -1) {
                    MYGAME.graphics.drawImage({
                        image: colors.white,
                        x: startX,
                        y: startY,
                        width: size,
                        height: size
                    });

                    startX += size;
                }
            }
            startY += size;
        }

        MYGAME.particles.drawParticles();
    }

    // A loop for updating and drawing the particles
    function particleLoop(timestamp) {
        var elapsedTime = (timestamp - lastTime) / 1000;
        lastTime = timestamp;

        updateParticles(elapsedTime);
        renderParticles(elapsedTime);

        if (animateParticles === true) {
            id = window.requestAnimationFrame(particleLoop);
        } else {
            cancelled = false;
            MYGAME.sounds['audio/Laser_Blaster.wav'].pause();
            MYGAME.sounds['audio/Laser_Blaster.wav'].currentTime = 0;
            game.removeRows();
            removedArray = game.checkRows();
            id = window.requestAnimationFrame(gameLoop); // Restart the game after particles have expired
        }
    }

    // Draws the gameboard
    function drawGameboard() {
        var board = game.getCurrentView(),
            i, j, startX = boardContainer.x - boardContainer.width / 2.1 + 15,
            startY = boardContainer.y - boardContainer.height / 2.1 + 10,
            initialX = startX;

        MYGAME.graphics.drawRect(boardContainer);

        for (i = 0; i < board.length; i++) {
            startX = initialX;
            for (j = 0; j < board[i].length; j++) {
                MYGAME.graphics.drawImage({
                    image: colors[board[i][j].color],
                    x: startX,
                    y: startY,
                    width: size,
                    height: size
                });
                startX += size;
            }
            startY += size;
        }
    }

    // Draws the next piece
    function drawNextPiece() {
        var piece = game.getNextPiece(), i, j,
            startX = nextPieceContainer.x - 50,
            startY = nextPieceContainer.y - 20,
            initialX = startX, pieceSize = 30;

        for (i = 0; i < piece.shape.length; i++) {
            startX = initialX;
            for (j = 0; j < piece.shape[i].length; j++) {
                if (piece.shape[i][j] === 1) {
                    MYGAME.graphics.drawImage({
                        image: colors[piece.color],
                        x: startX,
                        y: startY,
                        width: pieceSize,
                        height: pieceSize
                    });
                }
                startX += 30;
            }
            startY += 30;
        }

    }

    // Calculates the best combination of moves for the current piece
    function updateAI(elapsedTime) {
        var currentId = game.getCurrentPiece().getInfo().id,
            best;

        if (currentId !== currentPieceId) {
            currentPieceId = currentId;
            best = MYGAME.ai.getBestMove(game.getCurrentView(), game.getCurrentPiece());
            blockMovingThreshold = 0;
            rotations = best.rotations;
            leftMoves = 6;
            rightMoves = best.right;

        } else {
            updatePosition(elapsedTime);
        }
    }

    // Moves the piece according to the best moves previously calculated,
    // only moves every 0.1 seconds for animation purposes (otherwise it would be really fast)
    function updatePosition(elapsedTime) {
        blockMovingThreshold += elapsedTime;
        if (blockMovingThreshold > 0.1) {
            if (rotations > 0) {
                rotateRight();
                rotations--;
            } else if (leftMoves > 0) {
                moveLeft();
                leftMoves--;
            } else if (rightMoves > 0) {
                moveRight();
                rightMoves--;
            } else {
                hardDrop();
            }

            blockMovingThreshold = 0;
        }
    }

    // Updates the game
    function update(elapsedTime) {
        var stats;
        if (removedArray.length === 0) {
            removedArray = game.update(elapsedTime);
            if (removedArray.length > 0) {
                cancelled = true;
                animateParticles = true;
            }
        } else if (removedArray.length > 0) {
            cancelled = true;
            animateParticles = true;
        }

        stats = game.getStats();

        scoreContainer.text = stats.score;
        levelContainer.text = stats.level + 1;
        rowsClearedContainer.text = stats.lines;

        if (game.getStatus() === true) {
            cancelled = true;
            gameOver = true;
        }
    }

    // Draws the flashing "DEMO MODE" text
    function drawDemoModeText(elapsedTime) {
        demoModeTime += elapsedTime;
        if (demoModeTime > 1) {
            demoModeTime = false;
            demoModeShow = !demoModeShow;
        }

        if (demoModeShow === true) {
            MYGAME.graphics.drawText(demoModeGraphic);
        }
    }

    function render(elapsedTime) {
        MYGAME.graphics.drawImage(backgroundImage);
        MYGAME.graphics.drawImage(logoImage);

        MYGAME.graphics.drawImage(scoreImage);
        MYGAME.graphics.drawRect(scoreContainer);
        MYGAME.graphics.drawText(scoreContainer);

        MYGAME.graphics.drawImage(levelImage);
        MYGAME.graphics.drawRect(levelContainer);
        MYGAME.graphics.drawText(levelContainer);

        MYGAME.graphics.drawImage(rowsClearedImage);
        MYGAME.graphics.drawRect(rowsClearedContainer);
        MYGAME.graphics.drawText(rowsClearedContainer);

        MYGAME.graphics.drawImage(nextPieceImage);
        MYGAME.graphics.drawRect(nextPieceContainer);

        drawNextPiece();

        drawGameboard();

        drawDemoModeText(elapsedTime);
    }

    function gameLoop(timestamp) {
        var elapsedTime = (timestamp - lastTime) / 1000;

        lastTime = timestamp;
        updateAI(elapsedTime);
        update(elapsedTime);
        render(elapsedTime);

        if (cancelled === false) {
            id = window.requestAnimationFrame(gameLoop);
        } else if (gameOver === true) {
            goBack();
        } else if (animateParticles === true) {
            startParticles();
        }
    }

    return {
        initialize: initialize,
        run: run
    };
}());
