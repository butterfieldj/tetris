MYGAME.screens['controls'] = (function () {
    'use strict';

    // Boxes, text and variables
    var logoImage = {
            height: 55,
            width: 790,
            x: 0,
            y: 0
        },
        backgroundImage = {
            height: 0,
            width: 0,
            x: 0,
            y: 0
        },
        moveRightImage = {
            height: 56,
            width: 490,
            x: 0,
            y: 0
        },
        moveLeftImage = {
            height: 56,
            width: 450,
            x: 0,
            y: 0
        },
        rotateRightImage = {
            height: 56,
            width: 581,
            x: 0,
            y: 0
        },
        rotateLeftImage = {
            height: 56,
            width: 540,
            x: 0,
            y: 0
        },
        softDropImage = {
            height: 56,
            width: 460,
            x: 0,
            y: 0
        },
        hardDropImage = {
            height: 56,
            width: 473,
            x: 0,
            y: 0
        },
        moveRightBox = {
            x: 0,
            y: 0,
            width: 80,
            height: 50,
            fillStyle: "#FFCC00",
            font: '10px Arial',
            altFill: "#000000"
        },
        moveLeftBox = {
            x: 0,
            y: 0,
            width: 80,
            height: 50,
            fillStyle: "#FFCC00",
            font: '10px Arial',
            altFill: "#000000"
        },
        softDropBox = {
            x: 0,
            y: 0,
            width: 80,
            height: 50,
            fillStyle: "#FFCC00",
            font: '10px Arial',
            altFill: "#000000"
        },
        hardDropBox = {
            x: 0,
            y: 0,
            width: 80,
            height: 50,
            fillStyle: "#FFCC00",
            font: '10px Arial',
            altFill: "#000000"
        },
        rotateRightBox = {
            x: 0,
            y: 0,
            width: 80,
            height: 50,
            fillStyle: "#FFCC00",
            font: '10px Arial',
            altFill: "#000000"
        },
        rotateLeftBox = {
            x: 0,
            y: 0,
            width: 80,
            height: 50,
            fillStyle: "#FFCC00",
            font: '10px Arial',
            altFill: "#000000"
        },
        instructions = {
            x: 0,
            y: 0,
            width: 150,
            height: 50,
            fillStyle: "#4A4A4A",
            font: '20px Arial',
            altFill: "#FFCC00",
            multiline: true,
            text: {
                "0": "Use the Arrow keys to navigate menu",
                "1": "Use the Enter key to select control to edit, then press desired key",
                "2": "Use Backspace to return to the Main Menu"
            }
        },
        currentIndex = 0,
        resetKeys = false,
        id,
        lastTime,
        gettingKey,
        cancelled,
        keyboard = MYGAME.Input.Keyboard;

    // When the game starts loading
    function initialize() {
        var canvasWidth = MYGAME.graphics.canvas.width,
            canvasHeight = MYGAME.graphics.canvas.height,
            yOffset = canvasHeight / 5,
            xOffset = canvasWidth / 12;

        logoImage.x = canvasWidth / 2;
        logoImage.y = logoImage.height / 2;
        logoImage.image = MYGAME.images['graphics/logo.png'];

        moveRightImage.width /= 2;
        moveRightImage.height /= 2;
        moveRightImage.x = xOffset + moveRightImage.width / 2;
        moveRightImage.y = yOffset * 2 - moveRightImage.height;
        moveRightImage.image = MYGAME.images['graphics/moveRight.png'];
        moveRightImage.altImage = MYGAME.images['graphics/moveRightSelected.png'];

        moveRightBox.x = xOffset * 6 - moveRightBox.width / 2;
        moveRightBox.y = moveRightImage.y;

        moveLeftImage.width /= 2;
        moveLeftImage.height /= 2;
        moveLeftImage.x = xOffset + moveLeftImage.width / 2;
        moveLeftImage.y = yOffset * 3 - moveLeftImage.height;
        moveLeftImage.image = MYGAME.images['graphics/moveLeft.png'];
        moveLeftImage.altImage = MYGAME.images['graphics/moveLeftSelected.png'];

        moveLeftBox.x = xOffset * 6 - moveLeftBox.width / 2;
        moveLeftBox.y = moveLeftImage.y;

        softDropImage.width /= 2;
        softDropImage.height /= 2;
        softDropImage.x = xOffset + moveLeftImage.width / 2;
        softDropImage.y = yOffset * 4 - softDropImage.height;
        softDropImage.image = MYGAME.images['graphics/softDrop.png'];
        softDropImage.altImage = MYGAME.images['graphics/softDropSelected.png'];

        softDropBox.x = xOffset * 6 - softDropBox.width / 2;
        softDropBox.y = softDropImage.y;

        hardDropImage.width /= 2;
        hardDropImage.height /= 2;
        hardDropImage.x = xOffset * 6.3 + hardDropImage.width / 2;
        hardDropImage.y = yOffset * 2 - hardDropImage.height;
        hardDropImage.image = MYGAME.images['graphics/hardDrop.png'];
        hardDropImage.altImage = MYGAME.images['graphics/hardDropSelected.png'];

        hardDropBox.x = xOffset * 11.9 - hardDropBox.width / 2;
        hardDropBox.y = hardDropImage.y;

        rotateRightImage.width /= 2;
        rotateRightImage.height /= 2;
        rotateRightImage.x = xOffset * 6.3 + rotateRightImage.width / 2;
        rotateRightImage.y = yOffset * 3 - rotateRightImage.height;
        rotateRightImage.image = MYGAME.images['graphics/rotateRight.png'];
        rotateRightImage.altImage = MYGAME.images['graphics/rotateRightSelected.png'];

        rotateRightBox.x = xOffset * 11.9 - rotateRightBox.width / 2;
        rotateRightBox.y = rotateRightImage.y;

        rotateLeftImage.width /= 2;
        rotateLeftImage.height /= 2;
        rotateLeftImage.x = xOffset * 6.3 + rotateLeftImage.width / 2;
        rotateLeftImage.y = yOffset * 4 - rotateLeftImage.height;
        rotateLeftImage.image = MYGAME.images['graphics/rotateLeft.png'];
        rotateLeftImage.altImage = MYGAME.images['graphics/rotateLeftSelected.png'];

        rotateLeftBox.x = xOffset * 11.9 - rotateLeftBox.width / 2;
        rotateLeftBox.y = rotateLeftImage.y;

        instructions.x = canvasWidth / 2;
        instructions.y = yOffset * 4.9 - instructions.height;

        backgroundImage.x = canvasWidth / 2;
        backgroundImage.y = canvasHeight / 2;
        backgroundImage.width = canvasWidth;
        backgroundImage.height = canvasHeight;
        backgroundImage.image = MYGAME.images['graphics/star_wars_background.jpg'];
    }

    // When the user wants to set controls
    function run() {
        keyboard.clearCommands();
        keyboard.keys = {};
        keyboard.registerCommand(KeyEvent.DOM_VK_UP, previousMenuItem);
        keyboard.registerCommand(KeyEvent.DOM_VK_DOWN, nextMenuItem);
        keyboard.registerCommand(KeyEvent.DOM_VK_RETURN, selectMenuOption);
        keyboard.registerCommand(KeyEvent.DOM_VK_BACK_SPACE, goBack);

        moveRightBox.fillStyle = "#FFCC00";
        moveLeftBox.fillStyle = "#FFCC00";
        rotateRightBox.fillStyle = "#FFCC00";
        rotateLeftBox.fillStyle = "#FFCC00";
        softDropBox.fillStyle = "#FFCC00";
        hardDropBox.fillStyle = "#FFCC00";

        updateKeyMapping();

        cancelled = false;
        resetKeys = false;
        gettingKey = false;

        lastTime = performance.now();
        id = window.requestAnimationFrame(gameLoop);
    }

    // Updates the mapping of the various controls for display on the page
    function updateKeyMapping() {
        var e;

        for (e in KeyEvent) {
            if (KeyEvent[e] === MYGAME.screens['game-play'].keys['right']) {
                moveRightBox.text = e.substring(7, e.length);
                break;
            }
        }

        for (e in KeyEvent) {
            if (KeyEvent[e] === MYGAME.screens['game-play'].keys['left']) {
                moveLeftBox.text = e.substring(7, e.length);
                break;
            }
        }

        for (e in KeyEvent) {
            if (KeyEvent[e] === MYGAME.screens['game-play'].keys['soft-drop']) {
                softDropBox.text = e.substring(7, e.length);
                break;
            }
        }

        for (e in KeyEvent) {
            if (KeyEvent[e] === MYGAME.screens['game-play'].keys['hard-drop']) {
                hardDropBox.text = e.substring(7, e.length);
                break;
            }
        }

        for (e in KeyEvent) {
            if (KeyEvent[e] === MYGAME.screens['game-play'].keys['rotate-right']) {
                rotateRightBox.text = e.substring(7, e.length);
                break;
            }
        }

        for (e in KeyEvent) {
            if (KeyEvent[e] === MYGAME.screens['game-play'].keys['rotate-left']) {
                rotateLeftBox.text = e.substring(7, e.length);
                break;
            }
        }
    }

    // Goes back to the main menu
    function goBack() {
        if (cancelled === false) {
            cancelled = true;
            if (id) {
                window.cancelAnimationFrame(id);
            }

            keyboard.clearCommands();
            MYGAME.game.showScreen('main-menu');
        }
    }

    // When the user chooses a key to set
    function selectMenuOption(elapsedTime, reset) {
        var operation;

        if (reset === false) {
            MYGAME.Input.Keyboard.stopListening(); // Stop any other listeners

	    // Find out which key it is
            switch (currentIndex) {
                case 0:
                    operation = "right";
                    moveRightBox.fillStyle = "#FFFFFF";
                    break;
                case 1:
                    operation = "left";
                    moveLeftBox.fillStyle = "#FFFFFF";
                    break;
                case 2:
                    operation = "soft-drop";
                    softDropBox.fillStyle = "#FFFFFF";
                    break;
                case 3:
                    operation = "hard-drop";
                    hardDropBox.fillStyle = "#FFFFFF";
                    break;
                case 4:
                    operation = "rotate-right";
                    rotateRightBox.fillStyle = "#FFFFFF";
                    break;
                case 5:
                    operation = "rotate-left";
                    rotateLeftBox.fillStyle = "#FFFFFF";
                    break;
            }

            cancelled = true;
            keyboard.listenForKey(operation); // Start listening for next key
        }
    }

    // Scrolling between items
    function nextMenuItem(elapsedTime, reset) {
        if (reset === true) {
            resetKeys = false;
        } else {
            if (resetKeys === false) {
                currentIndex++;
                if (currentIndex >= 6) {
                    currentIndex = 0;
                }
            }
            resetKeys = true;
        }
    }

    // Scrolling between menu items
    function previousMenuItem(elapsedTime, reset) {
        if (reset === true) {
            resetKeys = false;
        } else {
            if (resetKeys === false) {
                currentIndex--;
                if (currentIndex <= -1) {
                    currentIndex = 5;
                }
            }
            resetKeys = true;
        }
    }

    // Collect input
    function collectInput(elapsedTime) {
        keyboard.update(elapsedTime);
    }

    // Update the key mapping
    function update(elapsedTime) {
        updateKeyMapping();
    }

    // Render the menu options, textboxes and text
    function render(elapsedTime) {
        MYGAME.graphics.drawImage(backgroundImage);
        MYGAME.graphics.drawImage(logoImage);
        MYGAME.graphics.drawImage(moveRightImage);
        MYGAME.graphics.drawImage(moveLeftImage);
        MYGAME.graphics.drawImage(softDropImage);
        MYGAME.graphics.drawImage(hardDropImage);
        MYGAME.graphics.drawImage(rotateRightImage);
        MYGAME.graphics.drawImage(rotateLeftImage);

        MYGAME.graphics.drawRect(moveRightBox);
        MYGAME.graphics.drawRect(moveLeftBox);
        MYGAME.graphics.drawRect(softDropBox);
        MYGAME.graphics.drawRect(hardDropBox);
        MYGAME.graphics.drawRect(rotateRightBox);
        MYGAME.graphics.drawRect(rotateLeftBox);

        MYGAME.graphics.drawText(moveRightBox);
        MYGAME.graphics.drawText(moveLeftBox);
        MYGAME.graphics.drawText(softDropBox);
        MYGAME.graphics.drawText(hardDropBox);
        MYGAME.graphics.drawText(rotateRightBox);
        MYGAME.graphics.drawText(rotateLeftBox);

        MYGAME.graphics.drawText(instructions);

        // Draw the selected menu option
        switch (currentIndex) {
            case 0:
                MYGAME.graphics.drawAltImage(moveRightImage);
                break;
            case 1:
                MYGAME.graphics.drawAltImage(moveLeftImage);
                break;
            case 2:
                MYGAME.graphics.drawAltImage(softDropImage);
                break;
            case 3:
                MYGAME.graphics.drawAltImage(hardDropImage);
                break;
            case 4:
                MYGAME.graphics.drawAltImage(rotateRightImage);
                break;
            case 5:
                MYGAME.graphics.drawAltImage(rotateLeftImage);
                break;
        }
    }

    // Main game loop
    function gameLoop(timestamp) {
        var elapsedTime = (timestamp - lastTime) / 1000;
        lastTime = timestamp;
        collectInput(elapsedTime);
        update(elapsedTime);
        render(elapsedTime);
        if (cancelled === false) {
            id = window.requestAnimationFrame(gameLoop);
        }
    }

    return {
        initialize: initialize,
        run: run,
        resetKeys: resetKeys
    };
}());
