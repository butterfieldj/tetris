MYGAME.screens['credits'] = (function () {
    'use strict';

    var logoImage = {
            height: 55,
            width: 790,
            x: 0,
            y: 0
        },
        backgroundImage = {
            height: 0,
            width: 0,
            x: 0,
            y: 0
        },
        creditsImage = {
            x: 0,
            y: 0,
            width: 500,
            height: 250
        },
        instructions = {
            x: 0,
            y: 0,
            width: 150,
            height: 50,
            fillStyle: "#4A4A4A",
            font: '20px Arial',
            altFill: "#FFCC00",
            multiline: true,
            text: {"0": "Use Backspace to return to the Main Menu"}
        },
        id,
        lastTime,
        cancelled,
        keyboard = MYGAME.Input.Keyboard;

    function initialize() {
        var canvasWidth = MYGAME.graphics.canvas.width,
            canvasHeight = MYGAME.graphics.canvas.height,
            yOffset = canvasHeight / 4;

        backgroundImage.image = MYGAME.images['graphics/storm_trooper_background.jpg'];
        backgroundImage.x = canvasWidth / 2;
        backgroundImage.y = canvasHeight / 2;
        backgroundImage.width = canvasWidth;
        backgroundImage.height = canvasHeight;

        creditsImage.image = MYGAME.images['graphics/creditsImage.png'];
        creditsImage.x = canvasWidth / 2;
        creditsImage.y = canvasHeight / 2;

        logoImage.x = canvasWidth / 2;
        logoImage.y = logoImage.height / 2;
        logoImage.image = MYGAME.images['graphics/logo.png'];

        instructions.x = canvasWidth / 2;
        instructions.y = yOffset * 4;
    }

    function run() {
        keyboard.clearCommands();
        keyboard.keys = {};

        keyboard.registerCommand(KeyEvent.DOM_VK_BACK_SPACE, goBack);
        cancelled = false;

        lastTime = performance.now();
        id = window.requestAnimationFrame(gameLoop);
    }

    function goBack() {
        cancelled = true;
        if (id) {
            window.cancelAnimationFrame(id);
        }

        keyboard.clearCommands();
        MYGAME.game.showScreen('main-menu');
    }

    function collectInput(elapsedTime) {
        keyboard.update(elapsedTime);
    }

    function render(elapsedTime) {
        MYGAME.graphics.drawImage(backgroundImage);
        MYGAME.graphics.drawImage(logoImage);
        MYGAME.graphics.drawImage(creditsImage);

        MYGAME.graphics.drawText(instructions);
    }

    function gameLoop(timestamp) {
        var elapsedTime = (timestamp - lastTime) / 1000;
        lastTime = timestamp;

        collectInput(elapsedTime);
        render(elapsedTime);

        if (cancelled === false) {
            id = window.requestAnimationFrame(gameLoop);
        }
    }

    return {
        initialize: initialize,
        run: run
    };
}());
