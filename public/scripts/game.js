// ------------------------------------------------------------------------------
// 
// This is the game object. Everything about the game is located in this object.
//
// ------------------------------------------------------------------------------

MYGAME.game = (function () {
    'use strict';

    var currentScreen;

    function showScreen(id) {
        currentScreen = id;
        MYGAME.screens[currentScreen].run();
    }

    //------------------------------------------------------------------
    //
    // This function performs the one-time game initialization.
    //
    //------------------------------------------------------------------
    function initialize() {
        var screen = null;
        //
        // Go through each of the screens and tell them to initialize
        for (screen in MYGAME.screens) {
            if (MYGAME.screens.hasOwnProperty(screen)) {
                MYGAME.screens[screen].initialize();
            }
        }
        // Make the main-menu screen the active one
        currentScreen = 'main-menu';
        showScreen(currentScreen);
    }

    return {
        initialize: initialize,
        showScreen: showScreen
    };
}());
