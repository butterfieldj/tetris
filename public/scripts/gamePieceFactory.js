MYGAME.pieceFactory = (function () {
    'use strict';

    var SHAPES = [
            [
                [0, 1, 0],
                [1, 1, 1],
                [0, 0, 0]
            ], [
                [0, 0, 1],
                [1, 1, 1],
                [0, 0, 0]
            ], [
                [1, 0, 0],
                [1, 1, 1],
                [0, 0, 0]
            ], [
                [0, 1, 1],
                [1, 1, 0],
                [0, 0, 0]
            ], [
                [1, 1, 0],
                [0, 1, 1],
                [0, 0, 0]
            ], [
                [0, 0, 0, 0],
                [1, 1, 1, 1],
                [0, 0, 0, 0],
                [0, 0, 0, 0]
            ], [
                [1, 1],
                [1, 1]
            ]],
        COLORS = [
            'purple',
            'orange',
            'blue',
            'green',
            'red',
            'cyan',
            'yellow'
        ];

    // Creates a random piece
    function createPiece() {
        var num = Math.floor((Math.random() * SHAPES.length));
        return MYGAME.piece({shape: SHAPES[num], color: COLORS[num], id: performance.now()});
    }

    return {
        createPiece: createPiece
    };
});
