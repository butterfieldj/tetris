MYGAME.gameboard = (function () {
    'use strict';

    var cols = 10,
        rows = 20,
        contents = [];

    function reset() {
        var arr = [],
            tempA,
            i,
            j;

        for (i = 0; i < rows; i++) {
            tempA = [];
            for (j = 0; j < cols; j++) {
                tempA.push({color: 'white', id: 0});
            }
            arr.push(tempA);
        }
        contents = arr;
    }

    function init() {
        reset();
    }

    function getContents() {
        return contents;
    }

    function setContents(grid) {
        contents = grid;
    }

    return {
        cols: cols,
        rows: rows,
        contents: getContents,
        setContents: setContents,
        reset: reset,
        init: init
    };
});
