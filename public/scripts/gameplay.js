MYGAME.screens['game-play'] = (function () {
    'use strict';

    var logoImage = {
            x: 0,
            y: 0,
            width: 789,
            height: 55
        },
        backgroundImage = {
            x: 0,
            y: 0,
            width: 0,
            height: 0
        },
        game,
        cancelled = false,
        keyboard = MYGAME.Input.Keyboard,
        id,
        lastTime,
        keys = {
            "left": KeyEvent.DOM_VK_A,
            "right": KeyEvent.DOM_VK_D,
            "rotate-left": KeyEvent.DOM_VK_Q,
            "rotate-right": KeyEvent.DOM_VK_E,
            "soft-drop": KeyEvent.DOM_VK_S,
            "hard-drop": KeyEvent.DOM_VK_W,
            "goBack": KeyEvent.DOM_VK_BACK_SPACE
        },
        boardContainer = {
            x: 320,
            y: 280,
            width: 220,
            height: 420,
            fillStyle: "#4A4A4A"
        },
        scoreImage = {
            x: boardContainer.x + boardContainer.width,
            y: 0,
            width: 150,
            height: 35
        },
        scoreContainer = {
            x: 720,
            y: 0,
            width: 150,
            height: 50,
            fillStyle: "#4A4A4A",
            font: '30px Arial',
            altFill: "#FFCC00",
            text: ""
        },
        levelImage = {
            x: scoreImage.x,
            y: 0,
            width: 140,
            height: 35
        },
        levelContainer = {
            x: 720,
            y: 0,
            width: 150,
            height: 50,
            fillStyle: "#4A4A4A",
            font: '30px Arial',
            altFill: "#FFCC00",
            text: ""
        },
        rowsClearedImage = {
            x: scoreImage.x,
            y: 0,
            width: 185,
            height: 53
        },
        rowsClearedContainer = {
            x: 720,
            y: 0,
            width: 150,
            height: 50,
            fillStyle: "#4A4A4A",
            font: '30px Arial',
            altFill: "#FFCC00",
            text: ""
        },
        nextPieceImage = {
            x: boardContainer.x + boardContainer.width + 100,
            y: 0,
            width: 300,
            height: 30
        },
        nextPieceContainer = {
            x: nextPieceImage.x,
            y: 0,
            width: 250,
            height: 100,
            fillStyle: "#4A4A4A"
        },
        pauseContainer = {
            x: 0,
            y: 0,
            width: 600,
            height: 200,
            fillStyle: "#4A4A4A",
            font: '30px Arial',
            altFill: "#FFCC00",
            multiline: true,
            text: ""
        },
        instructions = {
            x: boardContainer.x - boardContainer.width,
            y: boardContainer.y,
            width: 180,
            height: boardContainer.height,
            fillStyle: "#4A4A4A",
            font: 'bold 15px Arial',
            altFill: "#FFCC00",
            multiline: true,
            text: ""
        },
        size = 20,
        keyThreshold,
        softDropPress,
        resetHard,
        resetRotateLeft,
        resetRotateRight,
        moveRightPress,
        moveLeftPress,
        gameOver,
        paused,
        currentLevel,
        playedLevelUp,
        levelUpIndex,
        animateParticles,
        removedArray,
        colors = [],
        levelUpSounds = [];

    var moveRight = function (elapsedTime) {
        moveRightPress += elapsedTime;
	// Only moves when the keypress time has passed a threshold (can hold key down to continue moving right)
        if (moveRightPress >= keyThreshold) {
            game.moveRight();
            moveRightPress = 0;
        }

        softDropPress = 0;
        moveLeftPress = 0;
    };

    var moveLeft = function (elapsedTime) {
        moveLeftPress += elapsedTime;
        if (moveLeftPress >= keyThreshold) {
            game.moveLeft();
            moveLeftPress = 0;
        }

        softDropPress = 0;
        moveRightPress = 0;
    };

    var rotateLeft = function (elapsedTime, reset) {
        if (resetRotateLeft === false) {
            game.rotateCounterClockwise();
            resetRotateLeft = true;
        }

        if (reset === true) {
            resetRotateLeft = false;
        }
    };

    var rotateRight = function (elapsedTime, reset) {
        if (resetRotateRight === false) {
            game.rotateClockwise();
            resetRotateRight = true;
        }

        if (reset === true) {
            resetRotateRight = false;
        }
    };

    var hardDrop = function (elapsedTime, reset) {
        if (resetHard === false) {
            removedArray = game.hardDrop();

            if (removedArray.length > 0) {
                cancelled = true;
                animateParticles = true;
            }
            resetHard = true;
        }

	// One keyup, the hard drop key can be pressed again (prevents many hard drops in a row)
        if (reset === true) {
            resetHard = false;
        }
    };

    var softDrop = function (elapsedTime) {
        softDropPress += elapsedTime;
        if (softDropPress >= keyThreshold) {
            removedArray = game.softDrop();
            if (removedArray.length > 0) {
                cancelled = true;
                animateParticles = true;
            }
            softDropPress = 0;
        }
    };

    var keyInUse = function (keyCode) {
        var key;

        for (key in keys) {
            if (keys[key] === keyCode) {
                return true;
            }
        }

        return false;
    };

    function initialize() {
        var canvasWidth = MYGAME.graphics.canvas.width,
            canvasHeight = MYGAME.graphics.canvas.height,
            yOffset = canvasHeight / 6;

        backgroundImage.image = MYGAME.images['graphics/vader_clone_background.jpg'];
        backgroundImage.x = canvasWidth / 2;
        backgroundImage.y = canvasHeight / 2;
        backgroundImage.width = canvasWidth;
        backgroundImage.height = canvasHeight;

        logoImage.image = MYGAME.images['graphics/logo.png'];
        logoImage.y = yOffset - logoImage.height;
        logoImage.x = canvasWidth / 2;

        scoreImage.image = MYGAME.images['graphics/score.png'];
        scoreImage.y = yOffset + 10;

        scoreContainer.y = scoreImage.y;

        levelImage.image = MYGAME.images['graphics/level.png'];
        levelImage.y = yOffset * 2 + 10;

        levelContainer.y = levelImage.y;

        rowsClearedImage.image = MYGAME.images['graphics/rowsCleared.png'];
        rowsClearedImage.y = yOffset * 3 + 10;

        rowsClearedContainer.y = rowsClearedImage.y;

        nextPieceImage.image = MYGAME.images['graphics/nextPiece.png'];
        nextPieceImage.y = yOffset * 4 + 10;

        nextPieceContainer.y = nextPieceImage.y + nextPieceContainer.height - 10;

        pauseContainer.x = canvasWidth / 2;
        pauseContainer.y = canvasHeight / 2;

        colors.white = MYGAME.images['graphics/backgroundSquare.png'];
        colors.purple = MYGAME.images['graphics/purpleSquare.png'];
        colors.blue = MYGAME.images['graphics/blueSquare.png'];
        colors.green = MYGAME.images['graphics/greenSquare.png'];
        colors.yellow = MYGAME.images['graphics/yellowSquare.png'];
        colors.cyan = MYGAME.images['graphics/cyanSquare.png'];
        colors.red = MYGAME.images['graphics/redSquare.png'];
        colors.orange = MYGAME.images['graphics/orangeSquare.png'];

        levelUpSounds.push(MYGAME.sounds['audio/chances.wav']);
        levelUpSounds.push(MYGAME.sounds['audio/chewy.wav']);
        levelUpSounds.push(MYGAME.sounds['audio/darkside.wav']);
        levelUpSounds.push(MYGAME.sounds['audio/emperor.wav']);
        levelUpSounds.push(MYGAME.sounds['audio/force.wav']);
        levelUpSounds.push(MYGAME.sounds['audio/r2d2.wav']);
        levelUpSounds.push(MYGAME.sounds['audio/try_not.wav']);

        keyThreshold = 0.1;
    }

    function run() {
        var key, textObj = {
                "0": "CONTROLS",
                "1": "",
                "2": "Pause / Exit:",
                "3": "Backspace",
                "4": "Left: ",
                "6": "Right: ",
                "8": "Rotate Left: ",
                "10": "Rotate Right: ",
                "12": "Soft Drop: ",
                "14": "Hard Drop: "
            },
            e;

        MYGAME.sounds['audio/imperial_march.wav'].pause();
        MYGAME.sounds['audio/imperial_march.wav'].currentTime = 0;

	// Start the background music
        MYGAME.sounds['audio/battleOfHeroes.wav'].currentTime = 0;
	MYGAME.sounds['audio/battleOfHeroes.wav'].play();

        keyboard.clearCommands();
        keyboard.keys = {};

	// Add key commands, and set them for rendering purposes
        for (key in keys) {
            if (key === "left") {
                keyboard.registerCommand(keys[key], moveLeft);
                for (e in KeyEvent) {
                    if (KeyEvent[e] === keys[key]) {
                        textObj["5"] = e.substring(7, e.length);
                    }
                }
            } else if (key === "right") {
                keyboard.registerCommand(keys[key], moveRight);
                for (e in KeyEvent) {
                    if (KeyEvent[e] === keys[key]) {
                        textObj["7"] = e.substring(7, e.length);
                    }
                }
            } else if (key === "rotate-left") {
                keyboard.registerCommand(keys[key], rotateLeft);
                for (e in KeyEvent) {
                    if (KeyEvent[e] === keys[key]) {
                        textObj["9"] = e.substring(7, e.length);
                    }
                }
            } else if (key === "rotate-right") {
                keyboard.registerCommand(keys[key], rotateRight);
                for (e in KeyEvent) {
                    if (KeyEvent[e] === keys[key]) {
                        textObj["11"] = e.substring(7, e.length);
                    }
                }
            } else if (key === "soft-drop") {
                keyboard.registerCommand(keys[key], softDrop);
                for (e in KeyEvent) {
                    if (KeyEvent[e] === keys[key]) {
                        textObj["13"] = e.substring(7, e.length);
                    }
                }
            } else if (key === "hard-drop") {
                keyboard.registerCommand(keys[key], hardDrop);
                for (e in KeyEvent) {
                    if (KeyEvent[e] === keys[key]) {
                        textObj["15"] = e.substring(7, e.length);
                    }
                }
            } else if (key === "goBack") {
                keyboard.registerCommand(keys[key], startPaused);
            }
        }

	// Set the controls text
        instructions.text = textObj;

        pauseContainer.text = {
            "0": "Game Paused",
            "1": "Do you want to exit?",
            "2": "All scores will be lost.",
            "3": "Press Y to exit or N to continue playing."
        };

        softDropPress = 0;
        resetHard = false;
        resetRotateLeft = false;
        resetRotateRight = false;
        moveRightPress = 0;
        moveLeftPress = 0;

        removedArray = [];

        paused = false;
        gameOver = false;
        animateParticles = false;
        currentLevel = 1;
        playedLevelUp = true;
        levelUpIndex = 0;

        game = MYGAME.tetris();
        game.init();

        cancelled = false;
        lastTime = performance.now();
        id = window.requestAnimationFrame(gameLoop);
    }

    // When the player resumes from pause, resets a few listeners and variables
    function restart() {
        var key;

        keyboard.clearCommands();
        keyboard.keys = {};

        for (key in keys) {
            if (key === "left") {
                keyboard.registerCommand(keys[key], moveLeft);
            } else if (key === "right") {
                keyboard.registerCommand(keys[key], moveRight);
            } else if (key === "rotate-left") {
                keyboard.registerCommand(keys[key], rotateLeft);
            } else if (key === "rotate-right") {
                keyboard.registerCommand(keys[key], rotateRight);
            } else if (key === "soft-drop") {
                keyboard.registerCommand(keys[key], softDrop);
            } else if (key === "hard-drop") {
                keyboard.registerCommand(keys[key], hardDrop);
            } else if (key === "goBack") {
                keyboard.registerCommand(keys[key], startPaused);
            }
        }

        softDropPress = 0;
        resetHard = false;
        resetRotateLeft = false;
        resetRotateRight = false;
        moveRightPress = 0;
        moveLeftPress = 0;

        paused = false;
        gameOver = false;
        animateParticles = false;

        removedArray = [];

        cancelled = false;
        lastTime = performance.now();
        id = window.requestAnimationFrame(gameLoop);
    }

    // Go back to the main menu
    function goBack() {
        MYGAME.sounds['audio/imperial_march.wav'].pause();
        MYGAME.sounds['audio/imperial_march.wav'].currentTime = 0;

	MYGAME.sounds['audio/battleOfHeroes.wav'].pause();
        MYGAME.sounds['audio/battleOfHeroes.wav'].currentTime = 0;

        keyboard.clearCommands();
        MYGAME.game.showScreen('main-menu');
        if (id) {
            window.cancelAnimationFrame(id);
        }
    }

    // Tells the game that is is pausing
    function startPaused() {
        paused = true;
        cancelled = true;
        keyboard.clearCommands();

        pauseContainer.text = {
            "0": "Game Paused",
            "1": "Do you want to exit?",
            "2": "All scores will be lost.",
            "3": "Press Y to exit or N to continue playing."
        };

        keyboard.registerCommand(KeyEvent.DOM_VK_Y, goBack);
        keyboard.registerCommand(KeyEvent.DOM_VK_N, restart);
    }

    // Draws the paused message
    function showPaused() {
        MYGAME.graphics.drawRect(pauseContainer);
        MYGAME.graphics.drawText(pauseContainer);
    }

    // Starts particle effect
    function startParticles() {
        var board = game.getCurrentView(),
            i, j, p, startX = boardContainer.x - boardContainer.width / 2.1 + 15,
            startY = boardContainer.y - boardContainer.height / 2.1 + 10,
            initialX = startX;

        MYGAME.particles.reset();
        for (i = 0; i < board.length; i++) {
            startX = initialX;

            for (j = 0; j < board[i].length; j++) {
                if (removedArray.indexOf(i) !== -1) {
                    MYGAME.graphics.drawImage({
                        image: colors.white,
                        x: startX,
                        y: startY,
                        width: size,
                        height: size
                    });

                    for (p = 0; p <= 10; p++) {
                        MYGAME.particles.createParticle({
                            image: MYGAME.images['graphics/smoke.png'],
                            size: 20,
                            x: startX,
                            y: startY,
                            duration: 0.25,
                            deviation: 0.1,
                            speed: 1,
                            speedDev: 0.5
                        });
                    }

                    for (p = 0; p <= 8; p++) {
                        MYGAME.particles.createParticle({
                            image: colors[board[i][j].color],
                            size: 5,
                            x: startX,
                            y: startY,
                            duration: 0.2,
                            deviation: 0.1,
                            speed: 80,
                            speedDev: 1
                        });
                    }

                    for (p = 0; p <= 8; p++) {
                        MYGAME.particles.createParticle({
                            image: MYGAME.images['graphics/fire.png'],
                            size: 15,
                            x: startX,
                            y: startY,
                            duration: 0.2,
                            deviation: 0.1,
                            speed: 2,
                            speedDev: 0.5
                        });
                    }

                    startX += size;
                }
            }

            startY += size;
        }

        lastTime = performance.now();
        id = window.requestAnimationFrame(particleLoop); // Start the particle loop
        MYGAME.sounds['audio/Laser_Blaster.wav'].play(); // Play the blaster sound
    }

    function updateParticles(elapsedTime) {
        animateParticles = MYGAME.particles.update(elapsedTime);
    }

    function renderParticles(elapsedTime) {
        var board = game.getCurrentView(),
            i, j, startX = boardContainer.x - boardContainer.width / 2.1 + 15,
            startY = boardContainer.y - boardContainer.height / 2.1 + 10,
            initialX = startX;

        render(elapsedTime);

        for (i = 0; i < board.length; i++) {
            startX = initialX;

            for (j = 0; j < board[i].length; j++) {
                if (removedArray.indexOf(i) !== -1) {
                    MYGAME.graphics.drawImage({
                        image: colors.white,
                        x: startX,
                        y: startY,
                        width: size,
                        height: size
                    });
                    startX += size;
                }
            }
            startY += size;
        }

        MYGAME.particles.drawParticles();
    }

    function particleLoop(timestamp) {
        var elapsedTime = (timestamp - lastTime) / 1000;
        lastTime = timestamp;

        updateParticles(elapsedTime);
        renderParticles(elapsedTime);

        if (animateParticles === true) {
            id = window.requestAnimationFrame(particleLoop);
        } else {
            cancelled = false;
            keyboard.keys = {};
            MYGAME.sounds['audio/Laser_Blaster.wav'].pause();
            MYGAME.sounds['audio/Laser_Blaster.wav'].currentTime = 0;
            game.removeRows();
            removedArray = game.checkRows();
            id = window.requestAnimationFrame(gameLoop);
        }
    }

    // If there was an error recording scores
    function error() {
        alert("ERROR RECORDING SCORES");
    }

    // Uses a POST to send the scores to the server
    function saveScore(score, name) {
        $.ajax({
            url: 'http://localhost:3000/v1/high-scores?name=' + name + '&score=' + score,
            cache: false,
            type: 'POST',
            error: function () {
                error();
            },
            success: function () {
                console.log("Scores recorded");
            }
        });
    }

    // When the user has lost
    function gameOverAnimation() {
        var name;

	// Pause background music
	MYGAME.sounds['audio/battleOfHeroes.wav'].pause();
        MYGAME.sounds['audio/battleOfHeroes.wav'].currentTime = 0;

	// Play the game over music
        MYGAME.sounds['audio/imperial_march.wav'].play();

        pauseContainer.text = {
            "0": "Game over",
            "1": "Final score: " + game.getStats().score,
            "2": "Final level: " + (game.getStats().level + 1) + " Total lines cleared: " + game.getStats().lines,
            "3": "Would you like to exit?",
            "4": "Press Y to exit or N to play again."
        };

	// Draw the game over message
        MYGAME.graphics.drawRect(pauseContainer);
        MYGAME.graphics.drawText(pauseContainer);

	// Get the name from the user for posting his/her high scores
        name = prompt("Enter your name for our high scores");
        if (name !== null) {
            saveScore(game.getStats().score, name);
        }

	// Wait for user input
        keyboard.clearCommands();
        keyboard.registerCommand(KeyEvent.DOM_VK_N, run);
        keyboard.registerCommand(KeyEvent.DOM_VK_Y, goBack);
    }

    function drawGameboard() {
        var board = game.getCurrentView(),
            i, j, startX = boardContainer.x - boardContainer.width / 2.1 + 15,
            startY = boardContainer.y - boardContainer.height / 2.1 + 10,
            initialX = startX;

        MYGAME.graphics.drawRect(boardContainer);

        for (i = 0; i < board.length; i++) {
            startX = initialX;
            for (j = 0; j < board[i].length; j++) {
                MYGAME.graphics.drawImage({
                    image: colors[board[i][j].color],
                    x: startX,
                    y: startY,
                    width: size,
                    height: size
                });
                startX += size;
            }
            startY += size;
        }
    }

    function drawNextPiece() {
        var piece = game.getNextPiece(), i, j,
            startX = nextPieceContainer.x - 50,
            startY = nextPieceContainer.y - 20,
            initialX = startX, pieceSize = 30;

        for (i = 0; i < piece.shape.length; i++) {
            startX = initialX;
            for (j = 0; j < piece.shape[i].length; j++) {
                if (piece.shape[i][j] === 1) {
                    MYGAME.graphics.drawImage({
                        image: colors[piece.color],
                        x: startX,
                        y: startY,
                        width: pieceSize,
                        height: pieceSize
                    });
                }
                startX += 30;
            }
            startY += 30;
        }

    }

    function collectInput(elapsedTime) {
        keyboard.update(elapsedTime);
    }

    // Called when the user reaches a new level, increment through preloaded sounds
    function playLevelUpSound() {
        levelUpSounds[levelUpIndex].currentTime = 0;
        levelUpSounds[levelUpIndex].play();
        levelUpIndex += 1;
        if (levelUpIndex === levelUpSounds.length) {
            levelUpIndex = 0;
        }
    }

    // Update the game, check for cleared rows, update stats, play level up sounds, etc.
    function update(elapsedTime) {
        var stats;
        if (removedArray.length === 0) {
            removedArray = game.update(elapsedTime);
            if (removedArray.length > 0) {
                cancelled = true;
                animateParticles = true;
            }
        } else if (removedArray.length > 0) {
            cancelled = true;
            animateParticles = true;
        }

        stats = game.getStats();

        scoreContainer.text = stats.score;
        levelContainer.text = stats.level + 1;
        rowsClearedContainer.text = stats.lines;


        if (currentLevel !== (stats.level + 1)) {
            currentLevel = (stats.level + 1);
            playedLevelUp = false;
        }

        if (playedLevelUp === false) {
            playLevelUpSound();
            playedLevelUp = true;
        }


        if (game.getStatus() === true) {
            cancelled = true;
            gameOver = true;
        }
    }

    function render(elapsedTime) {
        MYGAME.graphics.drawImage(backgroundImage);
        MYGAME.graphics.drawImage(logoImage);

        MYGAME.graphics.drawImage(scoreImage);
        MYGAME.graphics.drawRect(scoreContainer);
        MYGAME.graphics.drawText(scoreContainer);

        MYGAME.graphics.drawImage(levelImage);
        MYGAME.graphics.drawRect(levelContainer);
        MYGAME.graphics.drawText(levelContainer);

        MYGAME.graphics.drawImage(rowsClearedImage);
        MYGAME.graphics.drawRect(rowsClearedContainer);
        MYGAME.graphics.drawText(rowsClearedContainer);

        MYGAME.graphics.drawImage(nextPieceImage);
        MYGAME.graphics.drawRect(nextPieceContainer);

        MYGAME.graphics.drawRect(instructions);
        MYGAME.graphics.drawText(instructions);

        drawNextPiece();

        drawGameboard();
    }

    function gameLoop(timestamp) {
        var elapsedTime = (timestamp - lastTime) / 1000;

        lastTime = timestamp;
        collectInput(elapsedTime);
        update(elapsedTime);
        render(elapsedTime);

        if (cancelled === false) {
            id = window.requestAnimationFrame(gameLoop);
        } else if (gameOver === true) {
            gameOverAnimation();
        } else if (paused === true) {
            showPaused();
        } else if (animateParticles === true) {
            startParticles();
        }
    }

    return {
        initialize: initialize,
        run: run,
        keys: keys,
        keyInUse: keyInUse
    };
}());
