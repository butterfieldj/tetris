MYGAME.graphics = (function () {
    'use strict';

    var that = {
        canvas: document.getElementById('canvas')
    };

    that.context = that.canvas.getContext('2d');

    // Draws an image with the center at x, y
    that.drawImage = function (spec) {
        that.context.drawImage(spec.image, (spec.x - (spec.width / 2)), (spec.y - (spec.height / 2)), spec.width, spec.height);
    }

    // Draws an alternate image (used for selected items in a menu) with the center at x, y
    that.drawAltImage = function (spec) {
        that.context.drawImage(spec.altImage, (spec.x - (spec.width / 2)), (spec.y - (spec.height / 2)), spec.width, spec.height);
    }

    // Draws a rect with the center at x, y
    that.drawRect = function (spec) {
        that.context.fillStyle = spec.fillStyle;
        that.context.fillRect((spec.x - (spec.width / 2)), (spec.y - (spec.height / 2)), spec.width, spec.height);
    }

    // Measures text height
    that.getTextHeight = function (spec) {
        var height;

        that.context.save();
        that.context.font = spec.font;
        that.context.fillStyle = spec.fillStyle;
        height = that.context.measureText('m').width;
        that.context.restore();

        return height;
    }

    // Computes the width of a string in a certain style
    that.getTextWidth = function (spec, key) {
        var width;

        that.context.save();
        that.context.font = spec.font;
        that.context.fillStyle = spec.fillStyle;
        if (spec.hasOwnProperty("multiline")) {
            width = that.context.measureText(spec.text[key]).width;
        } else {
            width = that.context.measureText(spec.text).width;
        }
        that.context.restore();

        return width;
    }

    // Draws text in the center
    that.drawText = function (spec) {
        var key, offset;
        that.context.textBaseline = "middle";
        that.context.font = spec.font;
        that.context.fillStyle = spec.altFill;
        if (spec.hasOwnProperty("multiline")) {
            offset = spec.height / that.getTextHeight(spec) + 7;
            for (key in spec.text) {
                that.context.fillText(spec.text[key], (spec.x - (that.getTextWidth(spec, key) / 2)), spec.y - (spec.height / 2) + offset);
                offset += that.getTextHeight(spec) + 10;
            }
        } else {
            that.context.fillText(spec.text, spec.x - (that.getTextWidth(spec) / 2), spec.y);
        }
    }

    // Draws a special component (used in particle animations)
    that.drawComponent = function (spec) {
        that.context.save();
        that.context.translate(spec.center.x, spec.center.y);
        if (spec.hasOwnProperty('rotation')) {
            that.context.rotate(spec.rotation * Math.PI / 180);
        }

        that.context.translate(-1 * spec.center.x, -1 * spec.center.y);

        if (spec.hasOwnProperty('size')) {
            that.context.drawImage(
                spec.image,
                spec.center.x - spec.size / 2,
                spec.center.y - spec.size / 2,
                spec.size,
                spec.size);
        }

        that.context.restore();
    }

    return that;

}());
