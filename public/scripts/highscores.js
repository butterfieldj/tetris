MYGAME.screens['high-scores'] = (function () {
    'use strict';

    var logoImage = {
            height: 55,
            width: 790,
            x: 0,
            y: 0
        },
        backgroundImage = {
            height: 0,
            width: 0,
            x: 0,
            y: 0
        },
        rankGraphic = {
            height: 0,
            width: 0,
            x: 0,
            y: 0,
            multiline: true,
            text: {"0": "Rank"},
            fillStyle: "#F1FC19",
            font: 'bold 20px Arial',
            altFill: "#FFCC00"
        },
        scoreGraphic = {
            height: 0,
            width: 0,
            x: 0,
            y: 0,
            multiline: true,
            text: {"0": "Score"},
            fillStyle: "#F1FC19",
            font: 'bold 20px Arial',
            altFill: "#FFCC00"
        },
        nameGraphic = {
            height: 0,
            width: 0,
            x: 0,
            y: 0,
            multiline: true,
            text: {"0": "Name"},
            fillStyle: "#F1FC19",
            font: 'bold 20px Arial',
            altFill: "#FFCC00"
        },
        instructions = {
            x: 0,
            y: 0,
            width: 150,
            height: 50,
            fillStyle: "#F1FC19",
            font: 'bold 20px Arial',
            altFill: "#FFCC00",
            multiline: true,
            text: {"0": "Use Backspace to return to the Main Menu"}
        },
        id,
        lastTime,
        cancelled,
        scoresReady,
        scores,
        keyboard = MYGAME.Input.Keyboard;

    function initialize() {
        var canvasWidth = MYGAME.graphics.canvas.width,
            canvasHeight = MYGAME.graphics.canvas.height,
            yOffset = canvasHeight / 14,
            xOffset = canvasWidth / 5;

        backgroundImage.image = MYGAME.images['graphics/vader_background.jpg'];
        backgroundImage.x = canvasWidth / 2;
        backgroundImage.y = canvasHeight / 2;
        backgroundImage.width = canvasWidth;
        backgroundImage.height = canvasHeight;

        logoImage.x = canvasWidth / 2;
        logoImage.y = logoImage.height / 2;
        logoImage.image = MYGAME.images['graphics/logo.png'];

        rankGraphic.x = xOffset;
        rankGraphic.y = canvasHeight / 2;
        rankGraphic.height = yOffset * 11;
        rankGraphic.width = xOffset;

        scoreGraphic.x = xOffset * 2;
        scoreGraphic.y = canvasHeight / 2;
        scoreGraphic.height = yOffset * 11;
        scoreGraphic.width = xOffset;

        nameGraphic.x = xOffset * 3.5;
        nameGraphic.y = canvasHeight / 2;
        nameGraphic.height = yOffset * 11;
        nameGraphic.width = xOffset;

        instructions.x = canvasWidth / 2;
        instructions.y = yOffset * 13;
    }

    function error() {
        alert("Error getting scores");
    }

    function onReceiveScores(data) {
        if (data.hasOwnProperty("error")) {
            error();
        } else {
            scores = data;
            scoresReady = true;
        }
    }

    // jQuery function to get the high scores from the server
    function getScores() {
        $.ajax({
            url: 'http://localhost:3000/v1/high-scores',
            cache: false,
            type: 'GET',
            error: function () {
                console.log("Error getting scores");
            },
            success: function (data) {
                onReceiveScores(data);
            }
        });
    }

    function run() {
        keyboard.clearCommands();
        keyboard.keys = {};

        keyboard.registerCommand(KeyEvent.DOM_VK_BACK_SPACE, goBack);
        cancelled = false;

        lastTime = performance.now();
        scoresReady = false;
        scores = [];
        getScores();
        id = window.requestAnimationFrame(gameLoop);
    }

    function goBack() {
        cancelled = true;
        if (id) {
            window.cancelAnimationFrame(id);
        }

        keyboard.clearCommands();
        MYGAME.game.showScreen('main-menu');
    }

    function collectInput(elapsedTime) {
        var i;
        keyboard.update(elapsedTime);
        if (scoresReady === true) {
            for (i = 1; i <= scores.length; i++) {
                rankGraphic.text[i] = i;
                scoreGraphic.text[i] = scores[i - 1].score;
                nameGraphic.text[i] = scores[i - 1].name;
            }
        }
    }

    function render(elapsedTime) {
        MYGAME.graphics.drawImage(backgroundImage);
        MYGAME.graphics.drawImage(logoImage);

        MYGAME.graphics.drawText(rankGraphic);
        MYGAME.graphics.drawText(scoreGraphic);
        MYGAME.graphics.drawText(nameGraphic);

        MYGAME.graphics.drawText(instructions);
    }

    function gameLoop(timestamp) {
        var elapsedTime = (timestamp - lastTime) / 1000;
        lastTime = timestamp;

        collectInput(elapsedTime);
        render(elapsedTime);

        if (cancelled === false) {
            id = window.requestAnimationFrame(gameLoop);
        }
    }

    return {
        initialize: initialize,
        run: run
    };
}());
