var MYGAME = {
    images: {},
    sounds: {},
    screens: {},

    status: {
        preloadRequest: 0,
        preloadComplete: 0
    }
};

//------------------------------------------------------------------
//
// Wait until the browser 'onload' is called before starting to load
// any external resources.  This is needed because a lot of JS code
// will want to refer to the HTML document.
//
//------------------------------------------------------------------
window.addEventListener('load', function () {
    console.log('Loading resources...');
    Modernizr.load([
        {
            load: [
                'preload!graphics/logo.png',
                'preload!graphics/newGame.png',
                'preload!graphics/newGameSelected.png',
                'preload!graphics/highscores.png',
                'preload!graphics/highscoresSelected.png',
                'preload!graphics/controls.png',
                'preload!graphics/controlsSelected.png',
                'preload!graphics/credits.png',
                'preload!graphics/creditsSelected.png',
                'preload!graphics/back.png',
                'preload!graphics/backSelected.png',
                'preload!graphics/star_wars_background.jpg',
                'preload!graphics/hyper_speed_background.jpg',
                'preload!graphics/storm_trooper_background.jpg',
                'preload!graphics/vader_background.jpg',
                'preload!graphics/jedi_fight_background.jpg',
                'preload!graphics/vader_clone_background.jpg',
                'preload!graphics/moveRight.png',
                'preload!graphics/moveRightSelected.png',
                'preload!graphics/moveLeft.png',
                'preload!graphics/moveLeftSelected.png',
                'preload!graphics/rotateLeft.png',
                'preload!graphics/rotateLeftSelected.png',
                'preload!graphics/rotateRight.png',
                'preload!graphics/rotateRightSelected.png',
                'preload!graphics/softDrop.png',
                'preload!graphics/hardDrop.png',
                'preload!graphics/softDropSelected.png',
                'preload!graphics/hardDropSelected.png',
                'preload!graphics/backgroundSquare.png',
                'preload!graphics/blueSquare.png',
                'preload!graphics/greenSquare.png',
                'preload!graphics/orangeSquare.png',
                'preload!graphics/cyanSquare.png',
                'preload!graphics/purpleSquare.png',
                'preload!graphics/redSquare.png',
                'preload!graphics/yellowSquare.png',
                'preload!graphics/score.png',
                'preload!graphics/nextPiece.png',
                'preload!graphics/creditsImage.png',
                'preload!graphics/gameOver.png',
                'preload!graphics/rowsCleared.png',
                'preload!graphics/level.png',
                'preload!graphics/fire.png',
                'preload!graphics/smoke.png',

                'preload!audio/Star_Wars_Theme.wav',
                'preload!audio/Laser_Blaster.wav',
                'preload!audio/imperial_march.wav',

                'preload!audio/chances.wav',
                'preload!audio/chewy.wav',
                'preload!audio/darkside.wav',
                'preload!audio/emperor.wav',
                'preload!audio/force.wav',
                'preload!audio/r2d2.wav',
                'preload!audio/try_not.wav',
		'preload!audio/battleOfHeroes.wav',

                'preload!scripts/random.js',
                'preload!scripts/input.js',
                'preload!scripts/graphics.js',
                'preload!scripts/particles.js',
                'preload!scripts/highscores.js',
                'preload!scripts/credits.js',
                'preload!scripts/piece.js',
                'preload!scripts/gamePieceFactory.js',
                'preload!scripts/gameboard.js',
                'preload!scripts/stats.js',
                'preload!scripts/Tetris.js',
                'preload!scripts/controls.js',
                'preload!scripts/mainmenu.js',
                'preload!scripts/gameplay.js',
                'preload!scripts/ai.js',
                'preload!scripts/attract.js',
                'preload!scripts/game.js'
            ],
            complete: function () {
                console.log('All files requested for loading...');
            }
        }
    ]);
}, false);

//
// Extend yepnope with our own 'preload' prefix that...
// * Tracks how many have been requested to load
// * Tracks how many have been loaded
// * Places images into the 'images' object
// * Places sounds into the 'sounds' object
yepnope.addPrefix('preload', function (resource) {
    console.log('preloading: ' + resource.url);

    MYGAME.status.preloadRequest += 1;
    var isImage = /.+\.(jpg|png|gif)$/i.test(resource.url);
    var isSound = /.+\.(mp3|wav)$/i.test(resource.url);
    resource.noexec = isImage || isSound;
    resource.autoCallback = function (e) {
        if (isImage) {
            var image = new Image();
            image.src = resource.url;
            MYGAME.images[resource.url] = image;
        } else if (isSound) {
            var sound = new Audio(resource.url);
            console.log(resource.url);
            MYGAME.sounds[resource.url] = sound;
        }
        MYGAME.status.preloadComplete += 1;

        //
        // When everything has finished preloading, go ahead and start the game
        if (MYGAME.status.preloadComplete === MYGAME.status.preloadRequest) {
            console.log('Preloading complete!');
            MYGAME.game.initialize();
        }
    };

    return resource;
});
