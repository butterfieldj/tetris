MYGAME.screens['main-menu'] = (function () {
    'use strict';

    var logoImage = {
            height: 55,
            width: 790,
            x: 0,
            y: 0
        },
        newGameImage = {
            height: 56,
            width: 435,
            x: 0,
            y: 0,
            optionIndex: 0
        },
        highScoreImage = {
            height: 50,
            width: 520,
            x: 0,
            y: 0,
            optionIndex: 1
        },
        controlImage = {
            height: 50,
            width: 400,
            x: 0,
            y: 0,
            optionIndex: 2
        },
        creditImage = {
            height: 50,
            width: 330,
            x: 0,
            y: 0,
            optionIndex: 3
        },
        backgroundImage = {
            height: 0,
            width: 0,
            x: 0,
            y: 0
        },
        instructions = {
            x: 0,
            y: 0,
            width: 150,
            height: 50,
            fillStyle: "#4A4A4A",
            font: '20px Arial',
            altFill: "#FFCC00",
            multiline: true,
            text: {
                "0": "Use the Arrow keys to navigate menu",
                "1": "Use the Enter key to select menu option"
            }
        },
        keyboard = MYGAME.Input.Keyboard,
        currentIndex = 0,
        id,
        lastTime,
        changeInterval = 40,
        idleTime,
        resetKeys,
        cancelled;

    function initialize() {
        var canvasWidth = MYGAME.graphics.canvas.width,
            canvasHeight = MYGAME.graphics.canvas.height,
            yOffset = canvasHeight / 6;


        logoImage.image = MYGAME.images['graphics/logo.png'];
        newGameImage.image = MYGAME.images['graphics/newGame.png'];
        newGameImage.altImage = MYGAME.images['graphics/newGameSelected.png'];
        highScoreImage.image = MYGAME.images['graphics/highscores.png'];
        highScoreImage.altImage = MYGAME.images['graphics/highscoresSelected.png'];
        controlImage.image = MYGAME.images['graphics/controls.png'];
        controlImage.altImage = MYGAME.images['graphics/controlsSelected.png'];
        creditImage.image = MYGAME.images['graphics/credits.png'];
        creditImage.altImage = MYGAME.images['graphics/creditsSelected.png'];
        backgroundImage.image = MYGAME.images['graphics/hyper_speed_background.jpg'];

	MYGAME.sounds['audio/battleOfHeroes.wav'].addEventListener('ended', function(){
		MYGAME.sounds['audio/battleOfHeroes.wav'].play();
	});

	MYGAME.sounds['audio/Star_Wars_Theme.wav'].addEventListener('ended', function(){
		MYGAME.sounds['audio/Star_Wars_Theme.wav'].play();
	});

        logoImage.x = canvasWidth / 2;
        logoImage.y = (yOffset - logoImage.height);

        newGameImage.x = canvasWidth / 2;
        newGameImage.y = (yOffset * 2 - newGameImage.height);

        highScoreImage.x = canvasWidth / 2;
        highScoreImage.y = (yOffset * 3 - highScoreImage.height);

        controlImage.x = canvasWidth / 2;
        controlImage.y = (yOffset * 4 - controlImage.height);

        creditImage.x = canvasWidth / 2;
        creditImage.y = (yOffset * 5 - creditImage.height);

        instructions.x = canvasWidth / 2;
        instructions.y = (yOffset * 6 - instructions.height);

        backgroundImage.x = canvasWidth / 2;
        backgroundImage.y = canvasHeight / 2;
        backgroundImage.width = canvasWidth;
        backgroundImage.height = canvasHeight;
    }

    function run() {
        MYGAME.graphics.drawImage(backgroundImage);
        MYGAME.graphics.drawImage(logoImage);
        MYGAME.graphics.drawImage(newGameImage);
        MYGAME.graphics.drawImage(highScoreImage);
        MYGAME.graphics.drawImage(controlImage);
        MYGAME.graphics.drawImage(creditImage);

        keyboard.clearCommands();
        keyboard.registerCommand(KeyEvent.DOM_VK_DOWN, nextMenuItem);
        keyboard.registerCommand(KeyEvent.DOM_VK_UP, previousMenuItem);
        keyboard.registerCommand(KeyEvent.DOM_VK_RETURN, selectMenuOption);

        resetKeys = false;
        cancelled = false;
        keyboard.keys = {};

        idleTime = 0;

        MYGAME.sounds['audio/Star_Wars_Theme.wav'].play();
        lastTime = performance.now();
        id = window.requestAnimationFrame(gameLoop);
    }

    function nextMenuItem(elapsedTime, reset) {
        if (reset === true) {
            resetKeys = false;
        } else {
            if (resetKeys === false) {
                currentIndex++;
                if (currentIndex >= 4) {
                    currentIndex = 0;
                }
            }
            resetKeys = true;
        }

        idleTime = 0;
    }

    function previousMenuItem(elapsedTime, reset) {
        if (reset === true) {
            resetKeys = false;
        } else {
            if (resetKeys === false) {
                currentIndex--;
                if (currentIndex <= -1) {
                    currentIndex = 3;
                }
            }
            resetKeys = true;
        }

        idleTime = 0;
    }

    function selectMenuOption(elapsedTime, reset) {
        var newScreen;
        if (reset === true) {
            resetKeys = false;
        } else {
            if (resetKeys === false) {
                switch (currentIndex) {
                    case 0:
                        newScreen = 'game-play';
                        break;
                    case 1:
                        newScreen = 'high-scores';
                        break;
                    case 2:
                        newScreen = 'controls';
                        break;
                    case 3:
                        newScreen = 'credits';
                        break;
                }

                MYGAME.sounds['audio/Star_Wars_Theme.wav'].pause();
                MYGAME.sounds['audio/Star_Wars_Theme.wav'].currentTime = 0;

                if (id) {
                    window.cancelAnimationFrame(id);
                }

                cancelled = true;

                MYGAME.game.showScreen(newScreen);
            }
            resetKeys = true;
        }
    }

    function collectInput(elapsedTime) {
        keyboard.update(elapsedTime);
    }

    function update(elapsedTime) {
        idleTime += elapsedTime;
        if (idleTime >= 10) {
            MYGAME.sounds['audio/Star_Wars_Theme.wav'].pause();
            MYGAME.sounds['audio/Star_Wars_Theme.wav'].currentTime = 0;

            if (id) {
                window.cancelAnimationFrame(id);
            }

            cancelled = true;

            MYGAME.game.showScreen('attract-mode'); // Starts attract mode
        }
    }

    function render(elapsedTime) {
        MYGAME.graphics.drawImage(backgroundImage);
        MYGAME.graphics.drawImage(logoImage);
        MYGAME.graphics.drawImage(newGameImage);
        MYGAME.graphics.drawImage(highScoreImage);
        MYGAME.graphics.drawImage(controlImage);
        MYGAME.graphics.drawImage(creditImage);
        MYGAME.graphics.drawText(instructions);

        switch (currentIndex) {
            case 0:
                MYGAME.graphics.drawAltImage(newGameImage);
                break;
            case 1:
                MYGAME.graphics.drawAltImage(highScoreImage);
                break;
            case 2:
                MYGAME.graphics.drawAltImage(controlImage);
                break;
            case 3:
                MYGAME.graphics.drawAltImage(creditImage);
                break;
        }
    }

    function gameLoop(timestamp) {
        var elapsedTime = (timestamp - lastTime) / 1000;
        lastTime = timestamp;
        collectInput(elapsedTime);
        update(elapsedTime);
        render(elapsedTime);
        if (cancelled === false) {
            id = window.requestAnimationFrame(gameLoop);
        }
    }

    return {
        initialize: initialize,
        run: run
    };
}());
