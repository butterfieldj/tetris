MYGAME.piece = (function (s) {
    'use strict';

    var shape = s.shape,
        color = s.color,
        id = s.id,
        loc = {x: 0, y: 5 - Math.floor(s.shape[0].length / 2.0)};

    function getCols() {
        return shape[0].length;
    }

    function setLoc(x, y) {
        loc.x = x;
        loc.y = y;
    }

    function setShape(sh) {
        shape = sh;
    }

    function getRows() {
        return shape.length;
    }

    function getInfo() {
        return {color: color, shape: shape, id: id, loc: loc};
    }

    function addToGrid(grid) {
        var i,
            j;

        for (i = 0; i < getRows(); i++) {
            for (j = 0; j < getCols(); j++) {
                if (shape[i][j]) {
                    if (loc.x + i < grid.length && loc.y + j < grid[i].length) {
                        grid[loc.x + i][loc.y + j].color = color;
                        grid[loc.x + i][loc.y + j].id = id;
                    }
                }
            }
        }
        return grid;
    }

    function check(grid, dx, dy) {
        var x = loc.x + dx,
            y = loc.y + dy,
            w = grid.length,
            h = grid[0].length,
            i,
            j;

        for (i = 0; i < getRows(); i++) {
            for (j = 0; j < getCols(); j++) {
                if (shape[i][j]) {
                    if (!(0 <= x + i && x + i < w && 0 <= y + j && y + j < h)
                        || (grid[x + i][y + j].color !== 'white'
                        && grid[x + i][y + j].id !== id
                        && grid[x + i][y + j].id !== 0)) {

                        return false;
                    }
                }
            }
        }

        return true;
    }

    function chainCheck(grid, dx, dy) {
        var x = loc.x + dx,
            y = loc.y + dy,
            w = grid.length,
            h = grid[0].length,
            i,
            j;

        for (i = 0; i < getRows(); i++) {
            for (j = 0; j < getCols(); j++) {
                if (shape[i][j]) {
                    if (!(0 <= x + i && x + i < w && 0 <= y + j && y + j < h)
                        || (grid[x + i][y + j] !== id
                        && grid[x + i][y + j] !== 0)) {

                        return false;
                    }
                }
            }
        }
        return true;
    }

    function rotate(dir) {

        var piece = [],
            i,
            j,
            arr;

        if (getCols() > 2) {
            if (dir === 'L') {
                for (i = getRows() - 1; i >= 0; i--) {
                    arr = [];
                    for (j = 0; j < getCols(); j++) {
                        arr.push(shape[j][i]);
                    }
                    piece.push(arr);
                }
            } else {
                for (i = 0; i < getRows(); i++) {
                    arr = [];
                    for (j = getCols() - 1; j >= 0; j--) {
                        arr.push(shape[j][i]);
                    }
                    piece.push(arr);
                }
            }
        } else {
            piece = shape;
        }
        return piece;
    }

    function rotateCounter() {
        shape = rotate('L');
    }

    function rotateClock() {
        shape = rotate('R');
    }

    function checkRotate(grid, dir) {
        var origShape = shape,
            doRotate = false;
        shape = rotate(dir);

        if (loc.y === grid.cols - getCols() || loc.y === (grid.cols - getCols() + 1)) {
            if (check(grid.contents(), 0, -1)) {
                if (getCols() === 4) { // This is for long piece XXXX
                    loc.y--;
                    if (check(grid.contents(), 0, -1)) {
                        loc.y--;
                        doRotate = true;
                    } else {
                        doRotate = false;
                    }
                } else {
                    if (loc.y === (grid.cols - getCols() + 1)) {
                        loc.y--;
                        // This prevents the error that was being thrown
                        // trying to set the color of a grid square that doesn't exist
                    }
                    doRotate = true;
                }
            } else {
                doRotate = false;
            }
        } else if (loc.y === -1 || loc.y === -2) {
            if (check(grid.contents(), 0, 1)) {
                loc.y++;
                if (getCols() === 4) { // This is for long piece XXXX
                    if (check(grid.contents(), 0, 1)) {
                        loc.y++;
                        doRotate = true;
                    } else {
                        doRotate = false;
                    }
                } else {
                    doRotate = true;
                }
            } else {
                doRotate = false;
            }
        } else {
            doRotate = check(grid.contents(), 0, 0);
        }

        shape = origShape;
        return doRotate;
    }

    function checkVirtualRotate(grid, dir) {
        var origShape = shape,
            doRotate = false;
        shape = rotate(dir);

        if (loc.y === grid[0].length - getCols() || loc.y === (grid[0].length - getCols() + 1)) {
            if (check(grid, 0, -1)) {
                if (getCols() === 4) { // This is for long piece XXXX
                    loc.y--;
                    if (check(grid, 0, -1)) {
                        loc.y--;
                        doRotate = true;
                    } else {
                        doRotate = false;
                    }
                } else {
                    if (loc.y === (grid[0].length - getCols() + 1)) {
                        loc.y--;
                        // This prevents the error that was being thrown
                        // trying to set the color of a grid square that doesn't exist
                    }
                    doRotate = true;
                }
            } else {
                console.log('Piece cannot be moved left');
                doRotate = false;
            }
        } else if (loc.y === -1 || loc.y === -2) {
            if (check(grid, 0, 1)) {
                loc.y++;
                if (getCols() === 4) { // This is for long piece XXXX
                    if (check(grid, 0, 1)) {
                        loc.y++;
                        doRotate = true;
                    } else {
                        doRotate = false;
                    }
                } else {
                    doRotate = true;
                }
            } else {
                doRotate = false;
            }
        } else {
            doRotate = check(grid, 0, 0);
        }

        shape = origShape;
        return doRotate;
    }

    function drop() {
        loc.x++;
    }

    function moveRight() {
        loc.y++;
    }

    function moveLeft() {
        loc.y--;
    }

    function clearPreviousPosition(grid) {
        var i,
            j;

        for (i = 0; i < grid.length; i++) {
            for (j = 0; j < grid[i].length; j++) {
                if (grid[i][j].id === id) {
                    grid[i][j].color = 'white';
                    grid[i][j].id = 0;
                }
            }
        }
    }

    return {
        cols: getCols,
        rows: getRows,
        getInfo: getInfo,
        addToGrid: addToGrid,
        check: check,
        chainCheck: chainCheck,
        checkRotate: checkRotate,
        checkVirtualRotate: checkVirtualRotate,
        drop: drop,
        setShape: setShape,
        moveRight: moveRight,
        moveLeft: moveLeft,
        rotateCounter: rotateCounter,
        rotateClock: rotateClock,
        setLoc: setLoc,
        clearPreviousPosition: clearPreviousPosition
    };
});
