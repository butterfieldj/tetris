MYGAME.stats = (function () {
    'use strict';

    var level = 0,
        score = 0,
        lines = 0;

    function reset() {
        level = 0;
        score = 0;
        lines = 0;
    }

    function init() {
        reset();
    }

    function addScore(cleared) {
        var p = [0, 40, 100, 300, 1200];
        score += (level + 1) * p[cleared];
    }

    function addRowsDropped(num) {
        score += num;
    }

    function checkLevelUp() {
        if (lines >= (level + 1) * 10) {
            level++;
        }
    }

    function getStats() {
        return {lines: lines, score: score, level: level};
    }

    function addLine() {
        lines++;
    }

    function getDropTime() {
        var time = 1;
        if (level > 10) {
            time = 0.25;
        } else {
            time -= level * 0.075;
        }
        return time;
    }

    return {
        reset: reset,
        init: init,
        addLine: addLine,
        addScore: addScore,
        checkLevelUp: checkLevelUp,
        getStats: getStats,
        addRowsDropped: addRowsDropped,
        getDropTime: getDropTime
    };
});
