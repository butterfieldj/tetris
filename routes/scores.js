var next = 1,
	fs = require('fs');

// Open file for editing later
fs.open('routes/score.txt', 'wx', function(){});

// Report all scores
exports.all = function(request, response) {
	var scores = [], parsedDoc;
	
	fs.readFile('routes/score.txt', 'utf8', function(err, data){
		if(err){
			console.log(err);
			return;
		} else {
			try{
				// Parse data in the file
				parsedDoc = JSON.parse(data);
				// Add scores to the scores array
				for(var key in parsedDoc){
					scores.push(parsedDoc[key]);
				}

				// Write and send response
				response.writeHead(200, {'content-type': 'application/json'});
				response.end(JSON.stringify(scores));
			} catch (err) {
				responsse.writeHead(200, {'content-type':'application/json'});
				response.end(JSON.stringify({"error":"Document empty or corrupted"}));
			}
		}
	});
};


// Add a new score to the server
exports.add = function(request, response) {
	var scores = [],
		nextId = 1,
		key, i;
	console.log('add new score called');
	console.log('Name: ' + request.query.name);
	console.log('Score: ' + request.query.score);

	fs.readFile('routes/score.txt', 'utf8', function(err, data){
		if(err){
			console.log(err);
			return;
		} else {
			try{
				// Parse data in the file
				parsedDoc = JSON.parse(data);
				// Add scores to the scores array
				for(key in parsedDoc){
					scores.push(parsedDoc[key]);
					nextId += 1;
				}

				scores.push({
					id : nextId,
					name : request.query.name,
					score : request.query.score
				});

				// Sort the scores by highest
				scores.sort(function(a, b) {
					return b.score - a.score;
				});

				// Set the ID of each score
				for(i = 0; i < scores.length; i++){
					scores[i].id = (i + 1);
				}

				// Take only the top 10 scores
				if(scores.length > 10){
					scores = scores.slice(0, 10);
				}

				// Write scores to file
				fs.writeFile('routes/score.txt', JSON.stringify(scores), function(err){
					if(err){
						console.log(err);
					}
				});

				// Send response
				response.writeHead(200);
				response.end();
			} catch (err) {
				response.writeHead(200);
				response.end();
			}
		}
	});
};





